'''
    Name: SolninjaA
    YouTube Channel: Solninja A
    Twitch Channel: iamsolninjaa
    Project Name: Take a break timer
'''
# import time
import time
# import webbrowser
import webbrowser
# import random
import random

# make urls (make sure to replace "Your link here" with your link to any webpage)
urls = ["Your link here", "Your link here", "Your link here"]
# random will randomly pick a url from the choices that you gave it above
url = urls[random.randint(0, 2)]

while True:
    # waiting 2 hours
    time.sleep(7200)
    # open a new window for webbrowser
    ('windows-default')
    # open a new tab if possible
    webbrowser.open_new_tab(url)
